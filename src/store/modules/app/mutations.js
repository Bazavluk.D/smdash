import { set, toggle } from '@/utils/vuex'

export default {
  setDrawer: set('drawer'),
  setNotification: set('notification'),
  setImage: set('image'),
  setColor: set('color'),
  toggleDrawer: toggle('drawer'),
  toggleNotification: toggle('notification')
}
