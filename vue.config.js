module.exports = {
  devServer: {
    overlay: true,
    proxy: {
      '/api/*': {
        target: 'http://smdfront.test',
        ws: true,
        changeOrigin: true
      }
    },
    public: 'localhost:8080',
    watchOptions: {
      poll: true
    },
    disableHostCheck: true
  }
}
